import log from "@ajar/marker";
import mysql from "mysql2/promise";

const connectDB = async (
    host: string,
    port: number,
    database: string,
    user: string,
    password: string
): Promise<void> => {
    const connection = await mysql.createConnection({
        host,
        port,
        database,
        user,
        password,
    });
    await connection.connect();
    log.magenta(" ✨ Connected to MySql DB ✨ ");
};

export default connectDB;
