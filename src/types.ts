export interface ISqlData {
    db_host: string;
    db_port: number;
    db_name: string;
    db_user_name: string;
    db_user_password: string;
}

interface IAddress {
    address_id: number;
    country_name: string;
    country_code: number;
    postal_code: number;
    city: string;
    region: string;
    street_name: string;
    street_number: number;
}

interface IAccount {
    account_id: number;
    currency: string;
    balance: number;
    status: "active" | "inactive";
    address: IAddress;
    type: "individual" | "business" | "family";
}

export interface IIndividual extends IAccount {
    individual_id: number;
    first_name: string;
    last_name: string;
    email: string;
}

export interface IBusiness extends IAccount {
    company_id: number;
    company_name: string;
    context: "payroll" | "treasury" | "investments";
}

export interface IFamily extends IAccount {
    context: "travel" | "morgage" | "emergency" | "savings" | "checking";
    owners: [IIndividual];
}

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Express {
        interface Request {
            id: string;
        }
    }
}
