import fs from "fs";
import path, { dirname } from "path";
import { fileURLToPath } from "url";
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { setId } from "./middleware/id.middleware.js";
import { logger } from "./middleware/logger.middleware.js";
import {
    loggerError,
    responseWithError,
    urlNotFound,
} from "./middleware/errors.handler.js";
import connectDB from "./db/mysql.connection.js";
import { ISqlData } from "./types.js";

// configuration
const __dirname = dirname(fileURLToPath(import.meta.url));
const config_path = path.resolve(__dirname, "../config.json");
const data = fs.readFileSync(config_path, { encoding: "utf8", flag: "r" });
const { host, port, sql, log_file_path, error_log_file_path } =
    JSON.parse(data);

class Api {
    private _app;

    constructor(private _HOST: string, private _PORT: number) {
        this._app = express();
    }

    init() {
        // middleware
        this._app.use(cors());
        this._app.use(morgan("dev"));
        this._app.use(express.json());
        this._app.use(setId);
        this._app.use(logger(log_file_path as string));

        // routing
        // this.routing();

        // error handling
        this.errorHandler();

        // start the express api server
        this.connectToDB().catch(console.log);
    }

    // private routing() {
    //     // this._app.use("/api/auth", auth_router);
    // }

    private errorHandler() {
        this._app.use("*", urlNotFound);
        this._app.use(loggerError(error_log_file_path as string));
        this._app.use(responseWithError);
    }

    private async connectToDB() {
        // connect to mongo db
        await connectDB(
            (sql as ISqlData).db_host,
            (sql as ISqlData).db_port,
            (sql as ISqlData).db_name,
            (sql as ISqlData).db_user_name,
            (sql as ISqlData).db_user_password
        );
        this._app.listen(this._PORT, this._HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${this._HOST}:${this._PORT} ✨ ⚡`
        );
    }
}

const app = new Api(host as string, port as number);
app.init();
