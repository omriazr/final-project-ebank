import express from "express";

const router = express.Router();

// transfer from a business account to a business account having the same currency

// transfer from a business account to an individual account having the same currency

// transfer from a family account to a business account having the same currency

// transfer from a business account to a business account with FX (accounts have a different currency)

export default router;
