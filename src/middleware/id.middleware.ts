/* eslint-disable @typescript-eslint/require-await */
import { NextFunction, Request, Response } from "express";
import v4 from "uuid";
const { v4: uuidv4 } = v4;

export const setId = async (
    req: Request,
    res: Response,
    next: NextFunction
): Promise<void> => {
    req.id = uuidv4();
    next();
};
