/* eslint-disable @typescript-eslint/await-thenable */
import { NextFunction, Request, Response, RequestHandler } from "express";

export default function raw(fn: RequestHandler) {
    return async function (
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<void> {
        try {
            await fn(req, res, next);
        } catch (err) {
            next(err);
        }
    };
}
