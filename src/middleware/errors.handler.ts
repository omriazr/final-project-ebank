/* eslint-disable @typescript-eslint/no-unused-vars */
import { NextFunction, Request, Response } from "express";
import fs from "fs/promises";

import HttpException from "../exceptions/http.exception.js";
import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";

// url not found
export const urlNotFound = (
    req: Request,
    res: Response,
    next: NextFunction
): void => {
    next(new UrlNotFoundException(req.path));
};

// log error
export const loggerError =
    (path: string) =>
    async (
        err: HttpException,
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        const newLog = `
    ${req.id}
    ${err.status} ${err.stack as string} \r\n
    `;
        await fs.appendFile(path, newLog);
        next(err);
    };

// response with error
export const responseWithError = (
    err: HttpException | UrlNotFoundException,
    req: Request,
    res: Response,
    next: NextFunction
): void => {
    const response = {
        status: err.status || 500,
        message: err.message || "something went wrong",
        stack: err.stack,
    };
    res.status(response.status).send({ response });
};
